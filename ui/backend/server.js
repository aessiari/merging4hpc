const express = require('express')
const assert = require('assert')
const util = require('util')
const bodyParser = require('body-parser')
const fs = require('fs')

function getMongoURI(path) {
    console.log(`Trying to get Mongo URI from file ${path}...`)
    var uri = fs.readFileSync(path, 'utf8')
    // remove whitespace, if present
    return uri.replace(/\s/g, '')
}

const PORT = process.env.SC_UI_PORT || 3000
const CONFIG_DIR = process.env.SC_CONFIG_DIR || process.env.HOME + '/.scconfig'
const MONGO_URI = getMongoURI(CONFIG_DIR + '/mongo_uri')
console.log(`MongoDB URI set to: ${MONGO_URI}`)
const DIR_STATIC_FRONTEND = require('path').resolve(__dirname, '../frontend')
// TODO change this with the appropriate value
// const DIR_STATIC_CAPSULE_DATA = '...'

const DB_NAME = "sciencecapsule"
const EVENTS_COLLECTION_NAME = "current_ui_event"
const NOTES_COLLECTION_NAME = "notes"

const app = express()

app.use(bodyParser.json({ limit: "50mb" }))

app.use(express.static(DIR_STATIC_FRONTEND))
// app.use('/capsule-data/', express.static(DIR_STATIC_CAPSULE_DATA))

function logMongoConnection (err, client) {
    if (err) console.log(`ERROR: Could not connect to MongoDB at ${MONGO_URI}`)
    else console.log(`Connected to client at URL: ${client.s.url}`)
    // assert.equal(err, null)
}

function LogMongoOperation (err, toPrint) {
    if (err) console.log(`ERROR: ${err}`)
    else {
        console.log(`operation successful!`)
        if (toPrint) console.log(toPrint)
    }
}

var MongoClient = require('mongodb').MongoClient

function fetchAllFromDB(req, res, dbName, collName) {
    MongoClient.connect(MONGO_URI, function(err, client) {
        logMongoConnection(err, client)
        var collection = client.db(dbName).collection(collName)
        var cursor = collection.find()

        cursor.toArray(function(err, docs) {
            LogMongoOperation(err, `Fetched ${docs.length} items`)
            res.send(docs)
            client.close()
        })
    })
}

// TODO consider extracting "saveAllToDB" to its own function, similarly to fetchAllFromDB
function saveNoteToDB(req, res) {
    var data = req.body
    console.log(util.inspect(data))
    res.json({request_data: data})
    MongoClient.connect(MONGO_URI, function(err, client) {
        logMongoConnection(err, client)
        var collection = client.db(DB_NAME).collection(NOTES_COLLECTION_NAME)

        collection.insertOne(data, function(err, result) {
            LogMongoOperation(err, util.inspect(result))
            client.close()
        })
    })
}

app.get('/events/', (req, res) => fetchAllFromDB(req, res, DB_NAME, EVENTS_COLLECTION_NAME))
app.get('/notes/', (req, res) => fetchAllFromDB(req, res, DB_NAME, NOTES_COLLECTION_NAME))
app.post('/notes/', saveNoteToDB)

app.listen(PORT, () => console.log(`Science Capsule UI backend listening on port ${PORT}!`))
