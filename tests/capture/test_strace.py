from pathlib import Path

import pytest
import pandas as pd


@pytest.fixture(scope='session')
def raw_data():
    path = Path(__file__).parent / 'data' / 'strace' / 'temp.json'

    return pd.read_json(path)


@pytest.fixture(scope='class')
def pipeline_params():
    from sc.capture.sources.strace import Analyzer

    return {'event_cls': Analyzer.event_model}


@pytest.fixture(scope='class')
def pipeline(pipeline_params):
    from sc.capture.sources.strace import RawEventsProcessingPipeline

    return RawEventsProcessingPipeline(**pipeline_params)


def test_raw_data_is_dataframe(raw_data):
    assert isinstance(raw_data, pd.DataFrame)


def test_raw_data_is_not_empty(raw_data):
    assert not raw_data.empty


@pytest.mark.usefixtures('raw_data', 'pipeline')
class TestAnalysisPipeline:

    @pytest.fixture(scope='class')
    def output(self, raw_data, pipeline):
        output = pipeline.run(raw_data)
        return output

    def test_output_is_not_empty(self, output):

        assert len(output) > 0

    def test_output_has_correct_properties(self, output):

        assert len(output) == 2
        proc_start, proc_end = output[0], output[1]

        for ev in [proc_start, proc_end]:
            assert ev.artifact.pid == 10761
            assert ev.artifact.exepath == '/bin/mv'
            assert ev.artifact.argv == ['mv', 'bar.txt', 'baz.tzt']

        assert proc_start.action == 'start'
        assert proc_end.action == 'exit'
