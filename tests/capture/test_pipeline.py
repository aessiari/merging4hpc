import pytest

from sc.capture.analysis import LinearPipeline as Pipeline
from sc.capture import exceptions


def double(x):
    return x * 2


def triple(x):
    return x * 3


def halve(x):
    return x / 2


@pytest.fixture
def steps():
    return [
        double,
        triple,
        halve
    ]


@pytest.fixture
def params():
    return {
        'foo': 'bar',
        'baz': 42
    }


@pytest.fixture
def pipeline(steps, params):
    return Pipeline(steps=steps, **params)


@pytest.fixture
def data_in():
    return 1


def test_pipeline_with_no_arguments_returns_input_data(data_in):
    data_out = Pipeline().run(data_in)
    assert data_out == data_in
    assert data_out is data_in


def test_cloning_preserves_pipeline_values(pipeline):

    cloned = pipeline.clone()

    assert cloned.steps == pipeline.steps
    assert cloned.steps is not pipeline.steps

    assert cloned.params == pipeline.params
    assert cloned.params is not pipeline.params


def test_slicing_returns_same_pipeline_with_subset_of_steps(pipeline):

    first_two = pipeline[:2]

    assert first_two.params == pipeline.params
    assert first_two.steps == pipeline.steps[:2]


def test_indexing_returns_step(pipeline):
    last_step = pipeline[-1]

    assert last_step == pipeline.steps[-1]


def test_indexing_with_invalid_type_raises(pipeline):

    with pytest.raises(TypeError):
        assert pipeline['foo']


def test_pipeline_with_valid_data_runs_without_errors(pipeline):

    data_in = 1
    expected_data_out = 3

    data_out, debug_info = pipeline.run_debug(data_in)
    assert not debug_info.has_errors
    assert len(debug_info) == len(pipeline)
    for step_info, step in zip(debug_info, pipeline):
        assert step_info.step == step
    assert data_out == expected_data_out


def test_wrapper_methods_give_same_results(pipeline, data_in):

    res_run_debug, _ = pipeline.run_debug(data_in)
    res_run = pipeline.run(data_in)
    res_call = pipeline(data_in)

    return res_run_debug == res_run == res_call


def test_pipeline_with_invalid_data_raises_error(pipeline):
    data_in = 'foo'
    key_of_step_with_error = 'halve'
    index_of_step_with_error = 2
    expected_error = TypeError

    with pytest.raises(exceptions.ProcessingError) as e:
        data_out, debug_info = pipeline.run_debug(data_in)
        assert len(e.debug_info) < len(pipeline), 'the pipeline should exit before running all the steps'
        assert key_of_step_with_error in str(e)
        assert str(index_of_step_with_error) in str(e)
        collected_errors = e.debug_info[key_of_step_with_error].collected_errors
        assert collected_errors
        assert isinstance(collected_errors[0], expected_error)

