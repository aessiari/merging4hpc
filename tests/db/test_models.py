import pytest

import pymodm
from sc.db import models


def make_example_model():
    class ExampleEnum(models.EventPropertyEnum):
        up = 'up'
        down = 'down'

    class ExampleEvent(models.Event):
        my_enum = models.EnumField(ExampleEnum)
        my_obj_proxy = models.ObjectProxyField()

        objects = models.UtilManager()

    return ExampleEvent


@pytest.fixture(scope='module')
def example_model():
    return make_example_model()


@pytest.fixture
def odm_model():

    class ODMModel(pymodm.MongoModel):
        name = pymodm.CharField()
        age = pymodm.IntegerField()

    return ODMModel


def test_base_odm_models_can_be_mocked(mock_db_uri, odm_model):

    pymodm.connect(mock_db_uri)

    obj = odm_model.objects.create(
        name='Foo Bar',
        age=42
    )

    qs = odm_model.objects.all()
    assert qs.count() == 1
    assert len(list(qs)) == 1
    assert qs.get({}) == obj
