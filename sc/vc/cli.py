"""
`sc.vc.cli`
====================================

.. currentmodule:: sc.vc.cli

:platform: Unix, Mac
:synopsis: Entry-point to Science Capsule snapshot CLI

.. moduleauthor:: Devarshi Ghoshal <dghoshal@lbl.gov>

"""

import argparse
from sc.vc.snapshot import TimeCapsule
import sys
import os
#from sc.constants import CONFIG_DIR


def _addArchiveParser(subparsers):
    parser_worker = subparsers.add_parser('archive',
                                          formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                          help=""" Create an archive of the current workspace """)

    parser_worker.set_defaults(action='archive')


def _addCreateParser(subparsers):
    parser_worker = subparsers.add_parser('create',
                                          formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                          help=""" Create a snapshot of the current workspace """)

    parser_worker.set_defaults(action='create')
    parser_worker.add_argument(dest='name', help='name for the snapshot')


def _addRemoveParser(subparsers):
    parser_worker = subparsers.add_parser('remove',
                                          formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                          help=""" Remove a snapshot of the current workspace """)

    parser_worker.set_defaults(action='remove')
    parser_worker.add_argument(dest='names', nargs='+', help='names for the snapshot')


def _addCheckoutParser(subparsers):
    parser_worker = subparsers.add_parser('get',
                                          formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                          help=""" Get a snapshot of the current workspace """)

    parser_worker.set_defaults(action='checkout')
    parser_worker.add_argument(dest='name', help='name for the snapshot')


def _addListParser(subparsers):
    parser_worker = subparsers.add_parser('list',
                                          formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                          help=""" List snapshots of the current workspace """)

    parser_worker.set_defaults(action='list')


def _addCheckParser(subparsers):
    parser_worker = subparsers.add_parser('check',
                                          formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                          help=""" Check snapshots of the current workspace """)

    parser_worker.set_defaults(action='check')
    parser_worker.add_argument(dest='name', help='name for the snapshot')
    parser_worker.add_argument('-v', '--verbose' ,help='verbose check', action='store_true')


def _addDiffParser(subparsers):
    parser_worker = subparsers.add_parser('diff',
                                          formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                          help=""" Diff snapshots of the current workspace """)

    parser_worker.set_defaults(action='diff')
    parser_worker.add_argument(dest='snapshot1', help='name for the first snapshot')
    parser_worker.add_argument('-c', '--compareto', help='name for the second snapshot', default='master')


def _addViewParser(subparsers):
    parser_worker = subparsers.add_parser('graph',
                                          formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                          help=""" Retrieves different views of a Snapshot """)

    parser_worker.set_defaults(action="graph")
    parser_worker.add_argument(dest='name', help='Name of the Science Capsule snapshot')
    parser_worker.add_argument(dest='graphtype', choices=['provenance', 'timeline'], help='type of view')
    #parser_worker.add_argument(dest='outfile', help='file where the output will be saved')
    parser_worker.add_argument('--fmt', help='output file format', default='svg')


##################################
def main():
    parser = argparse.ArgumentParser(description="",
                                     prog="sc-snapshot",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    subparsers = parser.add_subparsers()
    _addArchiveParser(subparsers)
    _addCreateParser(subparsers)
    _addRemoveParser(subparsers)
    _addCheckoutParser(subparsers)
    _addListParser(subparsers)
    _addCheckParser(subparsers)
    _addDiffParser(subparsers)
    #_addViewParser(subparsers)

    args = parser.parse_args()
    if len(args.__dict__) == 0:
        parser.print_usage()
        sys.exit(1)

    cwd = os.getcwd()
    action = args.action

    time_capsule = TimeCapsule(cwd, cwd)

    if action == 'archive':
        time_capsule.create_archive()
    elif action == 'create':
        name = args.name
        time_capsule.create_snapshot(name)
    elif action == 'remove':
        names = args.names
        time_capsule.remove_snapshot(names)
    elif action == 'checkout':
        name = args.name
        time_capsule.get_snapshot(name)
    elif action == 'list':
        time_capsule.list_snapshots()
    elif action == 'check':
        name = args.name
        verbose = args.verbose
        time_capsule.check_snapshot(name, verbose)
    elif action == 'diff':
        snap1 = args.snapshot1
        snap2 = args.compareto
        time_capsule.snapshot_diff(snap1, snap2)
    # elif action == 'graph':
    #     name = args.name
    #     view = args.viewtype
    #     #outfile = args.outfile
    #     fmt = args.fmt
    #     SC_OUTDIR = os.path.join(CONFIG_DIR, 'graphs')
    #     if not os.path.exists(SC_OUTDIR):
    #         os.makedirs(SC_OUTDIR)
    #     outfile = os.path.join(SC_OUTDIR, view)
    #     time_capsule.get_view(name, view, outfile, fmt)
    else:
        print("Invalid action!")

    time_capsule.shutdown()


##################################
if __name__ == '__main__':
    main()
