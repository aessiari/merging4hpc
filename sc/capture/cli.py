import argparse
import logging
from pathlib import Path
import sys

from sc.capture.core import Settings
from sc.capture.api import run_capture
from sc.capture import exceptions, util
from sc.logging import setup_logging


LOG = logging.getLogger('sc.capture.cli')


def run(args):
    setup_logging()

    settings = Settings.from_config()
    sources = settings.sources

    util.register_stop_signals()

    try:
        run_capture(
            sources=sources,
            settings=settings,
            monitor=args.monitor,
            ingest=args.ingest
        )
    except (KeyboardInterrupt, exceptions.ReceivedStopSignal) as e:
        LOG.info('Received stop signal %s', e)
        sys.exit()


def populate_argument_parser(parser: argparse.ArgumentParser):
    parser.add_argument(
        '--monitor',
        action='store_true',
        default=None,
        help='Enable real-time monitoring for event capture.'
    )
    parser.add_argument(
        '--no-ingest',
        # if given, we store the false value as a non-negated internal value to avoid double negations
        action='store_false', dest='ingest',
        help='Do not ingest events (including analysis of existing raw events)',
    )
    # parser.add_argument(
    #     '-s', '--event-sources',
    #     nargs='*'
    # )
    # parser.add_argument(
    #     '-d', '--monitored-dirs',
    #     nargs='*'
    # )
    # parser.add_argument(
    #     '-p', '--monitored-pids',
    #     nargs='*'
    # )

    parser.set_defaults(
        dispatch_func=run
    )

    return parser


def get_settings_from_args(args):
    return {
        'monitored_dirs': [Path(d).absolute() for d in args.monitored_dirs],
        'recursive': True,
        'monitored_pids': [int(pid) for pid in args.monitored_pids],
        'sources': list(args.event_sources)
    }


def main():
    parser = argparse.ArgumentParser()
    populate_argument_parser(parser)
    args = parser.parse_args()

    args.dispatch_func(args)


if __name__ == "__main__":
    main()
