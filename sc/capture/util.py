import functools
import io
import logging
import signal
import sys

import pandas as pd

from sc.capture import exceptions


LOG = logging.getLogger(__name__)


def _handle_stop_signals(sig_num, frame):
    raise exceptions.ReceivedStopSignal(sig_num)


def register_stop_signals():
    stop_signals = [
        signal.SIGINT,
        signal.SIGTERM,
    ]
    if sys.platform == 'win32':
        stop_signals.append(signal.SIGBREAK)

    for sig in stop_signals:
        LOG.debug('Registering stop signal %s (code=%d)', signal.Signals(sig), sig)
        signal.signal(sig, _handle_stop_signals)


def create_objects_from_dataframe(df, f_row_to_obj, **kwargs):
    if df.empty:
        return []

    apply_opts = {
        'axis': 1,  # apply function row-wise
        'result_type': 'reduce',  # ensure that the result is 1-dim
    }

    return (df
            .apply(f_row_to_obj, **apply_opts, **kwargs)
            .to_list()
           )


def get_timestamp_from_epoch(s, unit='s'):
    return (pd.to_datetime(s, origin='unix', unit=unit)
            .dt.tz_localize('UTC')
           )


def read_csv_from_series(ser, **kwargs):
    """
    Return a DataFrame from a Series containing text lines in CSV format.
    """
    csv_str = '\n'.join(ser.astype(str).values)
    return pd.read_csv(io.StringIO(csv_str), header=None, **kwargs)


def merge_csv_column(df, col, **kwargs):
    """
    Return a DataFrame after merging in columns extracted from the CSV data contained in one of its original columns `col`.
    """
    df_from_csv_column = df[col].pipe(read_csv_from_series, **kwargs)
    return df.join(df_from_csv_column)


def equals_shifted(df, shift=0, subset=None):
    """
    shift = +1 -> prev
    shift = -1 -> next
    """
    to_compare = df if subset is None else df[subset]
    shifted = to_compare.shift(shift)

    return (to_compare == shifted).all(axis='columns')


def differs_from_prev(df, subset=None):
    return ~equals_shifted(df, shift=1, subset=subset)


def get_chunk_id(is_start, start_from=0):

    return (is_start
            .cumsum()
            # TODO check offset adjustments
            .pipe(lambda s: s - s.min() + start_from)
            .astype(int)
    )


def assign_chunks(df, name='chunk_id', is_chunk_start=differs_from_prev, **kwargs):

    is_chunk = is_chunk_start(df, **kwargs)
    chunk_id = get_chunk_id(is_chunk)
    return df.assign(**{name: chunk_id})


def get_position_in_group(grp):
    r = pd.Series(index=list(range(len(grp))))
    if len(grp) == 1:
        r.iloc[0] = 'single'
    else:
        r.iloc[0] = 'start'
        r.iloc[-1] = 'stop'
    return r


def _get_series_from_scalar(indexed, val=None):
    """
    Return a Series whose values are `val` with the same index as the input object.
    """
    return pd.Series([val]*len(indexed), index=indexed.index)


class RegexParser:
    def __init__(self, field, regex=None, filter_func=None):
        self.field = field
        self.regex = regex
        _passthrough = functools.partial(_get_series_from_scalar, val=True)
        self.filter_func = filter_func or _passthrough

    def extract(self, df):
        target = df[self.filter_func][self.field]
        return target.str.extract(self.regex, expand=True)

    def extract_and_join(self, df):
        extracted = self.extract(df)
        return df.join(extracted, how='inner')


def expand_str_series_to_df(s, names=None):

    max_len = s.str.len().max()
    if pd.isnull(max_len):
        max_len = 0
    str_indices = range(max_len)
    names = names or str_indices

    df = pd.DataFrame(index=s.index, columns=names)

    for name, str_idx in zip(names, str_indices):
        df[name] = s.str.get(str_idx)

    return df


class CoalesceAdjacent:
    class c:
        is_distinct = 'is_distinct'
        chunk_id = 'chunk_id'
        first = 'first'
        size = 'size'
        duration = 'duration'
        n_coalesced = 'n_coalesced'

    def __init__(self, distinct=None, timestamp_column=None, timedelta_max=0):
        self.c_distinct = distinct or []
        self.c_timestamp = timestamp_column
        self.timedelta_max = timedelta_max

    def is_distinct(self, df):
        prev = df.shift(1)
        res = pd.DataFrame(index=df.index)

        for col in self.c_distinct:
            res[col] = (df[col] != prev[col])
        for col in [self.c_timestamp]:
            # abs() is just a precaution since these should be sorted
            timedelta = (df[col] - prev[col]).abs()
            res[col] = timedelta.dt.total_seconds() > self.timedelta_max

        return res.any(axis='columns')

    def get_aggregations(self, df):
        def delta_max_min(x):
            return x.max() - x.min()

        agg = {
            col: (col, self.c.first)
            for col in df
        }

        agg[self.c.duration] = (self.c_timestamp, delta_max_min)
        any_col = df.columns[0]
        agg[self.c.n_coalesced] = (any_col, self.c.size)

        return agg

    def coalesce(self, df):
        assigns = {
            self.c.is_distinct: self.is_distinct,
            self.c.chunk_id: lambda d: d[self.c.is_distinct].cumsum()
        }

        aggregations = self.get_aggregations(df)

        return (df
                .assign(**assigns)
                .groupby(self.c.chunk_id)
                .agg(**aggregations)
               )

    def __call__(self, df, **kwargs):
        return self.coalesce(df)
