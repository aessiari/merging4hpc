import subprocess
import threading
import time

from sc.capture import base, exceptions


class SubprocessOutputMonitor(base.Monitor):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.cli_args = []

        self._popen = None
        self._reader = None

    @property
    def subprocess_kwargs(self):
        return dict(stdout=subprocess.PIPE, text=True)

    def start(self):

        self.log.info('Starting...')
        self._popen = subprocess.Popen(self.cli_args, **self.subprocess_kwargs)
        self._reader = threading.Thread(target=self.iter_lines, args=(self._popen, self.process_line))

        self._reader.start()

    def check_status(self):
        poll_res = self._popen.poll()
        is_running_ok = poll_res is None

        if not is_running_ok:
            exit_code = poll_res
            status_msg = f'Terminated with exit code {exit_code}'
            raise exceptions.MonitorStatusException(status_msg)

    def iter_lines(self, proc, process_line):
        for line in iter(proc.stdout.readline, ''):
            line = line.strip()
            process_line(line)

    def process_line(self, line):
        event = self.event_manager.create(
            timestamp_ns=time.time_ns(),
            event_data=line
        )

    def stop(self, timeout=1):
        self.log.info('Stopping process')
        # TODO wait for timeout s before terminating?
        self._popen.terminate()
        self.log.info('Stopping auxiliary thread')
        self._reader.join(timeout)
        self.log.info('Done')

