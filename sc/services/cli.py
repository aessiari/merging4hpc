import argparse
import logging


LOG = logging.getLogger(__name__)


def populate_services_parser(parser):
    parser.add_argument('action', metavar='<action>', nargs='?', help=
        'Monitoring action to perform. Leave empty for interactive use, '
        'or use the `help` action to see all available actions.'
    )
    parser.add_argument('action_args', nargs=argparse.REMAINDER, help='Arguments to pass to the action')
    parser.add_argument('--inspect-processes', action='store_true', default=False, help='Show information about running internal processes and exits.')

    def _dispatch_with_supervisor(parsed_args):
        from sc.services.supervisor import _run_supervisorctl
        from sc.services import inspect

        ctl_args = []

        if parsed_args.inspect_processes:
            inspect._display_internal_processes()

        else:
            if parsed_args.action is not None:
                ctl_args += [parsed_args.action]
            else:
                LOG.info('No arguments passed. supervisorctl will be started in interactive mode')

            ctl_args += parsed_args.action_args

            _run_supervisorctl(*ctl_args)

    parser.set_defaults(dispatch_func=_dispatch_with_supervisor)


# used to test this functionality independently from the top-level `sc` entry point
if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='sc.services')

    populate_services_parser(parser)

    parsed_args = parser.parse_args()
    LOG.debug(parsed_args)

    try:
        parsed_args.dispatch_func(parsed_args)
    except KeyboardInterrupt:
        LOG.info('Received CTRL-C: exiting')
